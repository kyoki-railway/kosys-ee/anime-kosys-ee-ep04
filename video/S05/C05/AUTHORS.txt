################################################################################
# This file is contributors list of this directory and its sub directories. 
# See <repository root>/AUHTORS.txt for all contributors.
#-------------------------------------------------------------------------------
# このファイルはこのディレクトリおよびサブディレクトリの貢献者リストです。
# すべての貢献者は、<リポジトリルート>/AUTHORS.txtをご覧ください。 
################################################################################
#
# Note: 
#   - Revision history is available at 
#     https://git.opap.jp/projects/KOSYS/repos/kosys-ep03  
#     https://git.opap.jp/projects/KOSYS/repos/kosys-ep03-part1  
#     https://git.opap.jp/projects/KOSYS/repos/kosys-ep03-part2  
#     https://git.opap.jp/projects/KOSYS/repos/kosys-ep03-part3  
#     https://git.opap.jp/projects/KOSYS/repos/kosys-ep03-part4  
#
# 注記: 
#   - 更新履歴は以下からご覧いただけます。 
#     https://git.opap.jp/projects/KOSYS/repos/kosys-ep03  
#     https://git.opap.jp/projects/KOSYS/repos/kosys-ep03-part1  
#     https://git.opap.jp/projects/KOSYS/repos/kosys-ep03-part2  
#     https://git.opap.jp/projects/KOSYS/repos/kosys-ep03-part3  
#     https://git.opap.jp/projects/KOSYS/repos/kosys-ep03-part4  
#

井二かける
玉虫型偵察器
### END AUTO-GENERATED LIST.  LEAVE THIS LINE HERE; SCRIPTS LOOK FOR IT. ここまで自動生成です。上記を編集しないでください ###
PiT	3Dモデル,テクスチャ作成
廣田智亮	背景作画
井二かける	3Dモデル改変,テクスチャ改変,3Dアニメーション設定,背景作画加工

# ※3Dモデル、背景作画、著作権譲受済み
